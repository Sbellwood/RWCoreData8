//
//  EmployeePicture.swift
//  EmployeeDirectory
//
//  Created by Skyler Svendsen on 6/8/18.
//  Copyright © 2018 Razeware. All rights reserved.
//

import UIKit
import Foundation
import CoreData

public class EmployeePicture: NSManagedObject {
}

extension EmployeePicture {
  @nonobjc public class func fetchReques() -> NSFetchRequest<EmployeePicture> {
    return NSFetchRequest<EmployeePicture>(entityName: "EmployeePicture")
  }
  
  @NSManaged public var picture: Data?
  @NSManaged public var employee: Employee?
}
